let prompt = require ("prompt-sync")();

function start() {

    let isValid;
    let input;
    do {
        displayOutput(0);
        input = getUserInput(0);
        isValid = validateUserInput(input, 0) && input != 3;
        if (isValid) {
            console.log("operations entering")
            continueOperations(input);
        } else {
            console.log("Ooops!, wrong input\n")
        }
    } while (isValid)

}




function getUserInput(caller) {
    let input;
    switch(caller) {
    case 0: // caller is the start function, and accepts the first user input for selection of the operation of his choice.
        input = prompt("->>>> ");
        input = parseInt(input);
        break;
    case 2: // caller is the 1st operation, or the findFactorialOfAverage();
        input = new Array(5);
        input[0] = prompt("First number ->>>> ");
        input[0] = parseInt(input[0]);
        input[1] = prompt("Second number ->>>> ");

        input[1] = parseInt(input[1]);
        input[2] = prompt("Third number ->>>> ");

        input[2] = parseInt(input[2]);
        input[3] = prompt("Fourth number ->>>> ");

        input[3] = parseInt(input[3]);
        input[4] = prompt("Fifth number ->>>> ");

        input[4] = parseInt(input[4]);
        input[5] = prompt("6th number ->>>> ");

        break;
    default:
        break;
    }
    return input;
}
function displayOutput(caller) {
    switch(caller) {
    case 0:
        console.log("Pick an operation:");
        console.log("1 - Do something");
        console.log("2 - Do something else");
        console.log("4 - random");
        console.log("5 - findCloser");
        console.log("6 - findPalindrome");
        console.log("3 - exit");
        console.log("make a selection:");
        break;
    case 2:
        console.log("Please provide 5 numbers, one at a time:");
        break;
    default:
        break;
    }
}
function validateUserInput(input, caller) {
    let isValid = false;
    switch(caller) {
    case 0: //caller is the start function, or the first user input
        isValid = true;
        break;
    case 2: //caller is the 2nd operation, or the findFactorialOfAverage();
        isValid = true;
        break;
    default:
        isValid = false;
        break;
    } 
    return isValid;
}
function continueOperations(operation) {
    if (operation == 1) {
        console.log("operation 1");
        makeCubes();
    } else if (operation == 2) {
        console.log("operation 2");
        findFactorialOfAverage();
    } else if (operation == 3) {
        console.log("exit");
    } else if (operation == 4) {
        console.log("operation random");
        randomIntStats();

    } else if (operation == 5) {
        console.log("operation guess number");
        guessNumber(); 
    } else if (operation == 6) {
        console.log("operation palindrome");
        findPalindrome();
    }
}

function findFactorialOfAverage() {
    displayOutput(2);
    let input = getUserInput(2);
    let average = makeAverage(input);
    average = parseInt(average);

    let leastDeviation = average;
    let next = 0;
    let index = 0;
    for (let elements of input) {
        next = average - elements;
        if (next < 0) {
            next *= -1;
        }
        if (next < leastDeviation) {
            leastDeviation = next;
            index = elements;
        }
    }
    let factorial = calculateFactorial(index);

    console.log("Average of 5 integers is: ", average);
    console.log("The integer with the least deviation from the average: ", index);
    console.log("The factorial of the integer with the least deviation from the average: ", factorial);
}

function calculateFactorial(number) {
    if (number === 0) {
        return 1;
    } else {
        return number * calculateFactorial(number - 1);
    }
}
function makeCubes() {
    const range = 500;
    let numbers = new Array();
    let counter= 2; // i pass from calculating the cube of 0 or 1 since in the first case it is 0 and the second is 1;
    let current = 8; // the first cube number is 8;

    while (current <= range) {
        numbers.push(current);
        ++counter;
        current = counter * counter * counter;
    }
    for (let elements of numbers) {
        console.log(elements);
    }
    console.log("sum-->", makeSum(numbers));
    console.log("average--->", makeAverage(numbers));
    console.log("statistical_variance--->", makeStatisticalVariance(numbers));
    console.log("deviation--->", makeStandardDeviation(numbers));
}

function makeSum(array) {
    let sum = 0;
    for (let elements of array) {
        sum += elements;
    }
    return sum;
}

function makeAverage(array) {
    let sum = makeSum(array);
    let length = array.length;
    const average = sum / length;
    return average;
}
function makeStatisticalVariance(array) {
    let mean = makeAverage(array);
    let length = array.length;
    let variance = 0;

    for (let elements of array) {
        variance += (elements - mean) * elements;
    }

    variance = variance / length;
    return variance;
}
function makeStandardDeviation(array) {
    let variance = makeStatisticalVariance(array);
    let deviation = Math.sqrt(variance);
    return deviation;
}
start();

function makeProduct(array) {
    let product = array[0];
    for (let i = 1; i < array.length; ++i) {
        product *= array[i];
    }
    return product;

}
function randomIntStats() {
    let list = new Array();

    for (let i = 0; i < 6; ++i) {
        list[i] = Math.floor(Math.random() * 101);
        console.log(list[i]);
    }

    let sum = makeSum(list);
    let variance = makeStatisticalVariance(list);
    let deviation = makeStandardDeviation(list);
    let product = makeProduct(list);
    console.log("sum---> ", sum);
    console.log("variance---> ", variance);
    console.log("deviation---> ", deviation);
    console.log("product---> ", product);
}

function guessNumber() {
    let target = Math.floor(Math.random() * 66);
    let current_guess = -1;
    let current_difference = 0;
    let previous_difference = 66;
    console.log("target----  ", target);

    do {
        current_guess = prompt("Enter guess: ");
        current_guess = parseInt(current_guess);
        current_difference = target - current_guess;
        if (current_difference < 0) {
            current_difference *= -1;
        }
        if (current_difference < previous_difference) {
            console.log("you are getting closer");
        } else {
            console.log("you are getting further away");
        }
        previous_difference = current_difference;

    } while (current_guess != target);
}

function findPalindrome() {
    let input = prompt("Enter a string---> ");

    let input_reversed = new Array(input.length);
    for (let i = 0; i < input.length; ++i) {
        input_reversed[i] = input[input.length - i -1];
    }
    input_reversed = input_reversed.join("");
    if (input_reversed == input.toString()) {
        console.log(`indeed ${input} is a palindorme, and its palindrome is ${input_reversed}`);
    } else {
        console.log(`sorry ${input} is not a palindrome`);
        findPalindrome();
    }
}
